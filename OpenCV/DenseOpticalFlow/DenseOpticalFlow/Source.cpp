#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2\calib3d.hpp"
#include "opencv2\"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;
using namespace std;
int main(int argc, char** argv)
{
    Mat frame1 = imread("/home/radford/Desktop/1.png");
    Mat frame2 = imread("/home/radford/Desktop/2.png");

    namedWindow("flow");
    Mat flow;

    calcOpticalFlowSF(frame1, frame2, flow, 3, 2, 4);

    Mat xy[2];
    split(flow, xy);

    //calculate angle and magnitude
    Mat magnitude, angle;
    cartToPolar(xy[0], xy[1], magnitude, angle, true);

    //translate magnitude to range [0;1]
    double mag_max;
    minMaxLoc(magnitude, 0, &mag_max);
    magnitude.convertTo(magnitude, -1, 1.0 / mag_max);

    //build hsv image
    Mat _hsv[3], hsv;
    _hsv[0] = angle;
    _hsv[1] = Mat::ones(angle.size(), CV_32F);
    _hsv[2] = magnitude;
    merge(_hsv, 3, hsv);

    //convert to BGR and show
    Mat bgr;//CV_32FC3 matrix
    cvtColor(hsv, bgr, COLOR_HSV2BGR);
    imshow("flow", bgr);
    waitKey(0);
}