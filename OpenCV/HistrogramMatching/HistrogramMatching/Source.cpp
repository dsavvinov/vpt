#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

/**
* @function main
*/
const double START_SCALE = 1.0,
             SCALE_STEP  = 0.1,
             END_SCALE  = 1.1;

inline void getHist(Mat const & src, Mat & hist)
{
    int h_bins = 50,
        s_bins = 60;
    int histSize[] = { h_bins, s_bins };
    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges, s_ranges };

    int channels[] = { 0, 1 };

    /// Compute the histograms:
    calcHist(&src, 1, channels, Mat(), hist, 2, histSize, ranges, true, false);
    normalize(hist, hist, 0, 1, NORM_MINMAX, -1, Mat());
}

struct Position {
    double compValue;
    int x, y, height, width;
    Position() {} 
    Position(double c, int x, int y, int h, int w) :
        compValue(c),
        x(x),
        y(y),
        height(h),
        width(w) 
       { }

};

inline bool cmp(Position const & p1, Position const & p2)
{
    return p1.compValue < p2.compValue;
}

inline double myCompareHist(SparseMat const & a, SparseMat const & b, int type)
{
    return compareHist(a, b, type);
}

inline void decomposeRegion(Mat subregion, Mat & ground, Mat & puck)
{
    int xm = subregion.rows / 2;
    int ym = subregion.cols / 2;
    int r = min(subregion.rows, subregion.cols) / 2;
    Mat mask = cv::Mat::zeros(subregion.rows, subregion.cols, CV_8UC1);
    circle(mask, Point(xm, ym), r, Scalar(255, 255, 255), -1, 8, 0);
    subregion.copyTo(puck, mask);
    bitwise_not(mask, mask);
    subregion.copyTo(ground, mask);
}

inline double getDiff(Mat const im_r, Mat const im_g, Mat const im_b, Mat const puck_r, Mat const puck_g, Mat const puck_b)
{
    double r_diff = myCompareHist(SparseMat(im_r), SparseMat(puck_r), CV_COMP_BHATTACHARYYA);
    //double g_diff = myCompareHist(SparseMat(im_g), SparseMat(puck_g), CV_COMP_BHATTACHARYYA);
    //double b_diff = myCompareHist(SparseMat(im_b), SparseMat(puck_b), CV_COMP_BHATTACHARYYA);
    double cmpVal = r_diff; // +g_diff * g_diff + b_diff * b_diff;
    return cmpVal;
}

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// Global variables

Mat src, src_gray, dst;


int main(int argc, char** argv)
{
    for (int i = 20; i <= 20; ++i)
    {
        Mat templ, img;
        string s = "Match_Cut 0" + to_string(i) + ".jpg";
        /// Load image
        img = imread(s, 1);
        templ = imread(argv[2], 1);
        if (!img.data || !templ.data)
        {
            return -1;
        }

        /// Separate the image in 3 places ( B, G and R )

        Mat puck(templ.rows, templ.cols, CV_32F),
            gnd(templ.rows, templ.cols, CV_32F),
            puck_r, puck_g, puck_b, im_r, im_g, im_b, gnd_r, gnd_g, gnd_b,
            lp_r, lp_g, lp_b, lg_r, lg_g, lg_b;
        
        cout << templ.rows << ' ' << templ.cols << endl;
        cvtColor(templ, templ, COLOR_BGR2HSV);
        cvtColor(img, img, COLOR_BGR2HSV);
        
    
        /*threshold(img, img, 100, 255, 0);
        threshold(templ, templ, 150, 255, 0);
        namedWindow("puck", WINDOW_NORMAL);
        imshow("puck", templ);*/ 
        //decomposeRegion(templ, gnd, puck);
        //getRGBHist(puck, puck_r, puck_g, puck_b);

        gnd = templ;
        getRGBHist(templ, gnd_r, gnd_g, gnd_b);

        vector <Position> p;
        Mat res = img;

        for (int i = 0; i < img.rows; i += 5)
        {
            cout << i << endl;
            for (int j = 0; j < img.cols; j += 5)
            {
                Position best(1e8, -1, -1, -1, -1);
                double cur_scale = START_SCALE;
                while (cur_scale < END_SCALE)
                {
                    int subregionRows = int(puck.rows * cur_scale + 0.5);
                    int subregionCols = int(puck.cols * cur_scale + 0.5);
                    int y1 = i,
                        y2 = i + subregionRows,
                        x1 = j,
                        x2 = j + subregionCols;
                    int xm = (x1 + x2) / 2;
                    int ym = (y1 + y2) / 2;
                    int r = min(subregionCols, subregionRows) / 2;
                    cur_scale += SCALE_STEP;
                    if (y2 >= img.rows || x2 >= img.cols)
                        continue;
                    Mat subregion(img, Rect(Point(x1, y1), Point(x2, y2)));
                    getRGBHist(subregion, lg_r, lg_g, lg_b);
                    double gndDiff = getDiff(lg_r, lg_g, lg_b, gnd_r, gnd_g, gnd_b);
                    double cmpVal = gndDiff;
                    Position cur(cmpVal, x1, y1, subregionRows, subregionCols);
                    if (cmp(cur, best))
                        best = cur;
                }
                //if (best.compValue < SCORE_THRESHOLD)
                    p.push_back(best);
            }
        }

        sort(p.begin(), p.end(), cmp);
        for (size_t i = 0; i < min((int)p.size(), 50); ++i)
        {
            cout << p[i].compValue << endl;
            rectangle(img, Rect(p[i].x, p[i].y, p[i].width, p[i].height), Scalar(255, 0, 0), 1);
        }

        // Big and messy research on WTF is going on
        
        Mat subregion(img, Rect(Point(518, 270), Point(530, 280)));
        Mat cr, cg, cb;
        getRGBHist(subregion, cr, cg, cb);
        cout << subregion.rows << ' ' << subregion.cols << endl;
        double gndDiff = getDiff(cr, cg, cb, gnd_r, gnd_g, gnd_b);
        for (int i = 0; i < 256; ++i)
            cout << cr.at<float>(i) << ' ';
        cout << endl;
        for (int i = 0; i < 256; ++i)
            cout << gnd_r.at<float>(i) << ' ';
        cout << endl;

        double cmpVal = gndDiff;
        namedWindow("ans", WINDOW_AUTOSIZE);
        namedWindow("puck", WINDOW_AUTOSIZE);
        imshow("ans", subregion);
        imshow("puck", templ);
        cout << cmpVal << endl;
       /*
        {
            int histSize = 256;
            int hist_w = 512; int hist_h = 400;
            int bin_w = cvRound((double)hist_w / histSize);

            Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

            /// Normalize the result to [ 0, histImage.rows ]
            normalize(gnd_b, gnd_b, 0, histImage.rows, NORM_MINMAX, -1, Mat());
            normalize(gnd_g, gnd_g, 0, histImage.rows, NORM_MINMAX, -1, Mat());
            normalize(gnd_r, gnd_r, 0, histImage.rows, NORM_MINMAX, -1, Mat());

            /// Draw for each channel
            for (int i = 1; i < histSize; i++)
            {
                line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(gnd_b.at<float>(i - 1))),
                    Point(bin_w*(i), hist_h - cvRound(gnd_b.at<float>(i))),
                    Scalar(255, 0, 0), 2, 8, 0);
                line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(gnd_g.at<float>(i - 1))),
                    Point(bin_w*(i), hist_h - cvRound(gnd_g.at<float>(i))),
                    Scalar(0, 255, 0), 2, 8, 0);
                line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(gnd_r.at<float>(i - 1))),
                    Point(bin_w*(i), hist_h - cvRound(gnd_r.at<float>(i))),
                    Scalar(0, 0, 255), 2, 8, 0);
            }
            /// Display
            namedWindow("Ground hist", CV_WINDOW_AUTOSIZE);
            imshow("Ground hist", histImage);
        }
        {
            int histSize = 256;
            int hist_w = 512; int hist_h = 400;
            int bin_w = cvRound((double)hist_w / histSize);

            Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

            /// Normalize the result to [ 0, histImage.rows ]
            normalize(puck_b, puck_b, 0, histImage.rows, NORM_MINMAX, -1, Mat());
            normalize(puck_g, puck_g, 0, histImage.rows, NORM_MINMAX, -1, Mat());
            normalize(puck_r, puck_r, 0, histImage.rows, NORM_MINMAX, -1, Mat());

            /// Draw for each channel
            for (int i = 1; i < histSize; i++)
            {
                line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(puck_b.at<float>(i - 1))),
                    Point(bin_w*(i), hist_h - cvRound(puck_b.at<float>(i))),
                    Scalar(255, 0, 0), 2, 8, 0);
                line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(puck_g.at<float>(i - 1))),
                    Point(bin_w*(i), hist_h - cvRound(puck_g.at<float>(i))),
                    Scalar(0, 255, 0), 2, 8, 0);
                line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(puck_r.at<float>(i - 1))),
                    Point(bin_w*(i), hist_h - cvRound(puck_r.at<float>(i))),
                    Scalar(0, 0, 255), 2, 8, 0);
            }
            namedWindow("Puck hist", CV_WINDOW_AUTOSIZE);
            imshow("Puck hist", histImage);
        }
        namedWindow("calcHist Demo", CV_WINDOW_AUTOSIZE);
        imshow("calcHist Demo", img);
        waitKey(0);*/
        
        
        // ... So i have no idea what's going on

        namedWindow("result", WINDOW_AUTOSIZE);
        imshow("result", img);
        //imwrite("_Match_Cut 0" + to_string(i) + "_m.jpg", img);
        

        /// Display
        

        waitKey(0);
    }

    return 0;
}