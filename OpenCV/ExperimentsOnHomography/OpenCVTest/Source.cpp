#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;
using namespace cv;

const float inlier_threshold = 2.5f; // Distance threshold to identify inliers
const float nn_match_ratio = 0.8f;   // Nearest neighbor matching ratio

int main(int argc, char** argv)
{
	String prefix = "Match_Cut ", suffix = ".jpg";
	int imgNum = 1;
	for (int imgNum = 1; imgNum < 100; ++imgNum)
	{
		String strNum1, strNum2;
		stringstream ss1, ss2;
		ss1 << setw(3) << setfill('0') << imgNum;
		strNum1 = ss1.str();
		ss2 << setw(3) << setfill('0') << imgNum + 1;
		strNum2 = ss2.str();

		Mat img1 = imread(prefix + strNum1 + suffix, IMREAD_GRAYSCALE);
		Mat img2 = imread(prefix + strNum2 + suffix, IMREAD_GRAYSCALE);
		/*Mat img2;
		Mat M = cv::Mat::eye(3, 3, CV_32F);
		M.at<float>(0, 2) = 200;
		M.at<float>(1, 2) = 100;
		warpPerspective(img1, img2, M, Size(img1.cols, img1.rows));*/

		vector <KeyPoint> img1_feats, img2_feats;
		vector <Point2f> img1_pts, img2_pts;

		FAST(img1, img1_feats, 100, true);
		FAST(img2, img2_feats, 100, true);

		KeyPoint::convert(img1_feats, img1_pts);
		KeyPoint::convert(img2_feats, img2_pts);
		
		Mat feats1_final, feats2_final;
		drawKeypoints(img1, img1_feats, feats1_final, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
		drawKeypoints(img2, img2_feats, feats2_final, Scalar::all(-1), DrawMatchesFlags::DEFAULT);

		Mat H;
		if (img1_pts.size() > img2_pts.size())
			img1_pts.resize(img2_pts.size());
		else
			img2_pts.resize(img1_pts.size());
		H = findHomography(img1_pts, img2_pts, CV_RANSAC);

		Mat img3;
		warpPerspective(img1, img3, H, cv::Size(img1.cols, img1.rows));
		imwrite(strNum2 + "_trans.jpg", img3);
		imwrite(strNum1 + "_feats.jpg", feats1_final);
		imwrite(strNum2 + "_feats.jpg", feats2_final);
	}
	/* imshow("Img1", img1);
	imshow("Img2", img2);
	imshow("img3", img3);
	waitKey();*/
	return 0;
}