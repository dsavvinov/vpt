#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

vector < vector <int> > g;
bool was[101000], isArticular[101000];
int tin[101000], tup[101000];
int n, timer;

void dfs(int cur, int parent)
{
	was[cur] = true;
	tin[cur] = tup[cur] = timer++;
	int childs = 0;

	int curT = tin[cur];
	for (int i = 0; i < g[cur].size(); ++i)
	{
		if (g[cur][i] == parent)
			continue;
		if (was[g[cur][i]])
			tup[cur] = min(tup[cur], tin[g[cur][i]]);
		else
		{
			dfs(g[cur][i], cur);
			tup[cur] = min(tup[cur], tup[g[cur][i]]);
			if (tup[g[cur][i]] >= tin[cur] && parent != - 1)
				isArticular[cur] = true;
			++childs;
		}
	}

	if (parent == -1 && childs > 1)
		isArticular[cur] = true;
}

int main()
{
	//freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
	int from, to;
	g.resize(100000);
	n = -1;
	while (cin >> from >> to)
	{
		g[from].push_back(to);
		g[to].push_back(from);
		n = max(max(from, to), n);
	}

	++n;
	g.resize(n);
	dfs(0, -1);
	
	for (int i = 0; i < n; ++i)
	{
		if (isArticular[i])
			cout << i << ' ';
	}
	return 0;
}