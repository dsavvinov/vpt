#include "ParticleFilter.hpp"
#include <algorithm>
#include <iostream>

void ParticleFilter::init(std::vector<cv::Mat> init_poses, int nParticles)
{
    poses = init_poses;
    // add additional poses randomly if necessary
    while ((int)poses.size() < nParticles)
    {
        int ind = rng.uniform(0, (int)poses.size());
        cv::Mat new_pose = poses[ind].clone();
        new_pose.at<double>(0) += getNoise();
        new_pose.at<double>(1) += getNoise();
        poses.push_back(new_pose);
    }
    weights.assign(nParticles, 0.0);
    this->nParticles = nParticles;
}

void ParticleFilter::predict()
{
    for (auto p : poses)
    {
        p.at<double>(0) = p.at<double>(0) + getNoise();
        p.at<double>(1) = p.at<double>(1) + getNoise();
    }
    return;
}

void ParticleFilter::evaluate(cv::Mat mouse_pos)
{
    for (size_t i = 0; i < poses.size(); ++i)
    {
        double dist = getDist(poses[i], mouse_pos);
        //std::cout << "Got dist " << dist << " for point " << poses[i].at<double>(0) << ' ' << poses[i].at<double>(1) <<
        //    "and mouse position " << mouse_pos.at<double>(0) << ' ' << mouse_pos.at<double>(1) << std::endl;
        weights[i] = 1.0/dist;
    }
}

void ParticleFilter::resample()
{
    using namespace std;
    vector < double > cumulative_w(nParticles);
    vector <cv::Mat> new_poses(nParticles);

    // cumulative_w[i] = sum over all weights from 0 to i (including i)
    cumulative_w[0] = weights[0];
    for (size_t i = 1; i < nParticles; ++i)
    {
        cumulative_w[i] = cumulative_w[i - 1] + weights[i];
    }

    /* Cumulative_w is non-increasing now. Let's emulate non-uniform distribution, based on weights:
     Let `sum' be total sum of all weights (note that it stored in last cumulative_w)
     Now we'll get uniformly random number from range of [0, sum] nParticles times, 
     and find lower bound of that number in cumulative_w. Then next item is what we choose as result of roll. */
    for (size_t i = 0; i < nParticles; ++i)
    {
        double roll = rng.uniform(0.0, cumulative_w.back());
        //cout << "Rolled dist " << roll << endl;
        int lower_bound_ind = lower_bound(cumulative_w.begin(), cumulative_w.end(), roll) - cumulative_w.begin();
        //cout << "Adding pose " << lower_bound_ind << " with dist " << weights[lower_bound_ind] * 1000 << endl;
        new_poses[i] = poses[lower_bound_ind].clone();
    }

    /*for (size_t i = 0; i < nParticles; ++i)
    {
        if (i > 3 * nParticles / 4)
            new_poses[i] = poses[rng.uniform(0, nParticles)].clone();
        else
            new_poses[i] = poses[rng.uniform(0, 10)].clone();
    }*/

    poses = new_poses;
}

ParticleFilter::ParticleFilter()
{
    rng = cv::RNG(1337);
}

int ParticleFilter::size()
{
    return nParticles;
}

double ParticleFilter::getNoise()
{
    double roll = rng.uniform(-MAX_DIST, MAX_DIST);
    return roll;
}

double ParticleFilter::getDist(cv::Mat a, cv::Mat b)
{
    double ax = a.at<double>(0),
        ay = a.at<double>(1),
        bx = b.at<double>(0),
        by = b.at<double>(1);
    return sqrt((ax - bx) * (ax - bx) + (ay - by) * (ay - by));
}

