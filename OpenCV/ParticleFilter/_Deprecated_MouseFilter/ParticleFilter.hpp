#pragma once
#include "opencv2\world.hpp"

const double MAX_DIST = 50;
struct ParticleFilter
{
    // Initial estimation
    void init(std::vector<cv::Mat>, int);

    // Get prediction
    void predict();

    // Evaluate predictions based on measurments
    void evaluate(cv::Mat);

    // Resample based on weights
    void resample();

    std::vector<cv::Mat> poses;
    std::vector<double> weights;
    ParticleFilter();

    int size();
private:
    double getNoise();
    double getDist(cv::Mat, cv::Mat);
    cv::RNG rng;
    int nParticles;
};
