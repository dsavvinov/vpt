#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"
#include <Windows.h>
#include "ParticleFilter.hpp"
#include <iostream>
#define drawCross( center, color, d )                                 \
line( img, Point( center.x - d, center.y - d ), Point( center.x + d, center.y + d ), color, 2, CV_AA, 0); \
line( img, Point( center.x + d, center.y - d ), Point( center.x - d, center.y + d ), color, 2, CV_AA, 0 )

using namespace cv;
using namespace std;
Point mousePos;
void callback(int event, int x, int y, int flags, void* userdata)
{
    if (event == EVENT_MOUSEMOVE)
    {
        mousePos.x = x;
        mousePos.y = y;
    }
}
int main()
{
    // Image to show mouse tracking
    Mat img(600, 800, CV_8UC3);
    
    namedWindow("particle mouse", WINDOW_AUTOSIZE);
    setMouseCallback("particle mouse", callback);
    imshow("particle mouse", img);

    ParticleFilter PF;
    vector<Mat> init_vector;
    Mat t = Mat(2, 1, CV_64F);
    t.at<double>(0) = 300;
    t.at<double>(1) = 400;
    init_vector.push_back(t);
    PF.init(init_vector, 100);

    //for (int I = 0; I < 3; ++I)
    for (;;)
    {
        img = Scalar::all(0);
        Mat measurement(2, 1, CV_64F);
        // First predict, to update the internal statePre variable
        PF.predict();
        
        // Get mouse point
        measurement.at<double>(0) = mousePos.x;
        measurement.at<double>(1) = mousePos.y;
        PF.evaluate(measurement);

        PF.resample();
        
        for (size_t i = 0; i < PF.size(); ++i)
        {
            Point cur = Point(PF.poses[i].at<double>(0), PF.poses[i].at<double>(1));
            drawCross(cur, Scalar(255, 255, 255), 1);
        }

        imshow("particle mouse", img);
        waitKey(10);
    }

    waitKey();
    return 0;
}