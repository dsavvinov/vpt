#pragma once
#include "opencv2\world.hpp"
void processVideo();    // REMOVE ON RELEASE

struct ParticleFilter
{
    // Initial estimation
    void init(std::vector<cv::Mat> init_poses, int particles_count, cv::Mat templ);

    // Get prediction
    void predict();

    // Evaluate predictions based on measurments
    void evaluateViaHists(cv::Mat const & image, cv::Mat const & templ);
    void evaluateViaMOG(cv::Mat const & image, cv::Mat const & templ, cv::Mat const & mask);
    void evaluateSegment(cv::Mat const & image, cv::Mat const & templ, cv::Mat const & mask);
    // Resample based on weights
    void resample();

    std::vector<cv::Mat> poses;
    std::vector<double> weights;
    ParticleFilter();
    int size();
    void reinit(cv::Mat image, cv::Mat templ, cv::Mat back_mask);

    cv::Mat getWeightedMin();
    cv::Mat getBestMatch();
    int best_match_ind;
    friend void processVideo();     // REMOVE ON RELEASE
private:
    double getBackgroundDistance(cv::Mat img, cv::Mat topleft);
    double getNoise();
    cv::RNG rng;
    int nParticles;
    void cumulativeResample();
    void gaussianResample();
    void segmentObjects(cv::Mat);
    cv::Mat objects;
    std::vector <int> objects_size;
    int template_size;
    bool isPointInImage(cv::Point const cell, cv::Mat const & image);
    int getObjectSize(cv::Mat const & image, cv::Point const & start);
    cv::Mat getObjectHist(int id, cv::Mat const & image, cv::Mat const & mask);
    std::vector < std::pair <cv::Point, cv::Point> > objects_bboxes;
    cv::Mat mask;
};

cv::Mat poseToSubregion(cv::Mat const & pose, cv::Mat const & templ, cv::Mat const & img);
