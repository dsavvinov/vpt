#include "ParticleFilter.hpp"
#include "HistogramMatching.hpp"
#include <algorithm>
#include <iostream>
#include <queue>
#include <set> // REMOVE ON RELEASE
#include "opencv2\world.hpp"
#include "opencv2\highgui.hpp"

const double EPS                 = 1e-6;
const double MAX_DIST            = 10;
const double MOG_WEIGHT          = 2;
const double GRAYLEVEL_THRESHOLD = 0.7;
const int    INF                 = (1 << 30);
const bool   SPEED_HEURISTIC_ON  = true;
const int    SIZE_THRESHOLD      = 3;

void ParticleFilter::init(std::vector<cv::Mat> init_poses, int nParticles, cv::Mat templ)
{
    using namespace cv;
    for (size_t i = 0; i < nParticles; ++i)
    {
        poses.push_back(init_poses[i % init_poses.size()].clone());
    }
    weights.assign(nParticles, 0.0);
    
    Mat mask = Mat::zeros(templ.size(), CV_8UC1);
    // count pixel size of puck on template
    for (int i = 0; i < templ.rows; ++i)
    {
        for (int j = 0; j < templ.cols; ++j)
        {
            cv::Point3_<uchar> * p = templ.ptr<cv::Point3_<uchar> >(i, j);
            double grayscale = (0.0722 * p->x + 0.7152 * p->y + 0.2126 * p->z) / 255.0;
            if (grayscale < GRAYLEVEL_THRESHOLD)
            {
                mask.at<char>(i, j) = 127;
                template_size++;
            }
        }
    }

    this->mask = mask;
    this->nParticles = nParticles;
}

void ParticleFilter::predict()
{
    for (auto p : poses)
    {
        // CONTROL: noise
        p.at<double>(0) = int(p.at<double>(0) + p.at<double>(2) + getNoise()) % 1080;
        p.at<double>(1) = int(p.at<double>(1) + p.at<double>(3) + getNoise()) % 1900;
        if (SPEED_HEURISTIC_ON)
        {
            p.at<double>(2) = int(p.at<double>(2) + getNoise() / 2);
            p.at<double>(3) = int(p.at<double>(3) + getNoise() / 2);
        }
        else
        {
            p.at<double>(2) = 0;
            p.at<double>(3) = 0;
        }
    }
    return;
}

void ParticleFilter::evaluateViaHists(cv::Mat const & img, cv::Mat const & templ)
{
    for (size_t i = 0; i < poses.size(); ++i)
    {
        cv::Mat subregion = poseToSubregion(poses[i], templ, img);
        double dist = getHistDistance(subregion, templ, mask);
        //std::cout << weights[i] << std::endl;
        weights[i] += 1 - dist;
        //weights[i] += dist;
        /*std::cout.precision(10);
        std::cout << dist << ' ';
      
        std::cout << dist << std::endl;
        cv::waitKey();*/
    }
}

double ParticleFilter::getBackgroundDistance(cv::Mat subregion, cv::Mat topleft)
{
    int best_diff = INF,
        closest_object_ind = -1;
    int x0 = topleft.at<double>(0),
        y0 = topleft.at<double>(1);

    for (size_t i = 0; i < subregion.rows; ++i)
        for (size_t j = 0; j < subregion.cols; ++j)
        {
            if (subregion.at<char>(i, j) != 0)
            {
                int cur_id   = objects.at<int>(x0 + i, y0 + j);
                
                int cur_size = objects_size[cur_id - 1];
                if (abs(template_size - cur_size) < best_diff)
                {
                    closest_object_ind = cur_id;
                    best_diff = abs(template_size - cur_size);
                }
            }
        }

    // filter too large or too small objects
    if (closest_object_ind == -1 || objects_size[closest_object_ind - 1] > SIZE_THRESHOLD * template_size ||
        objects_size[closest_object_ind - 1] < template_size / double(SIZE_THRESHOLD) )
        return 0;

    return 1 - best_diff / (SIZE_THRESHOLD * template_size);
}

bool ParticleFilter::isPointInImage (cv::Point const cell, cv::Mat const & image)
{
    return cell.x >= 0 && cell.x < image.rows &&
           cell.y >= 0 && cell.y < image.cols;
}
int ParticleFilter::getObjectSize(cv::Mat const & mask, cv::Point const & start)
{
    // segment objects as connected component of '1' in image using BFS
    using namespace std;
    using namespace cv;

    int object_id = static_cast<int>(objects_size.size()) + 1;
    // arrays for checking neighbours of cell
    int dx[] = { -1, 1,  0, 0 };
    int dy[] = {  0, 0, -1, 1 };

    Point top_left, bottom_right;
    top_left.x = top_left.y = INF;
    bottom_right.x = bottom_right.y = -INF;
    queue < Point > q;
    q.push(start);
    int pixels_count = 1;
    objects.at<int>(start.x, start.y) = object_id;
    while (!q.empty())
    {
        Point cur = q.front();
        top_left.x = min(top_left.x, cur.x);
        top_left.y = min(top_left.y, cur.y);
        bottom_right.x = max(bottom_right.x, cur.x);
        bottom_right.y = max(bottom_right.y, cur.y);
        q.pop();
        for (int i = 0; i < 4; ++i)
        {
            Point next = Point(cur.x + dx[i], cur.y + dy[i]);
            if (isPointInImage(next, mask) && objects.at<int>(next.x, next.y) == 0 &&
                mask.at<char>(next.x, next.y) != 0)
            {
                q.push(next);
                pixels_count++;
                objects.at<int>(next.x, next.y) = object_id;
            }
        }
    }
    objects_bboxes.push_back(make_pair(top_left, bottom_right));
    return pixels_count;
}

cv::Mat ParticleFilter::getObjectHist(int obj_id, cv::Mat const & image, cv::Mat const & back_mask)
{
    using namespace cv;
    int x1 = objects_bboxes[obj_id - 1].first.x,
        y1 = objects_bboxes[obj_id - 1].first.y,
        x2 = objects_bboxes[obj_id - 1].second.x,
        y2 = objects_bboxes[obj_id - 1].second.y;
    Rect bbox = Rect(Point(y1, x1), Point(y2, x2));
    Mat subregion = Mat(image, bbox).clone();
    Mat mask = Mat(back_mask, bbox).clone();
    Mat tmp = Mat::zeros(subregion.size(), subregion.type());
    subregion.copyTo(tmp, mask);
    namedWindow("test", CV_WINDOW_NORMAL);
    imshow("test", tmp);
    return getHist(subregion, mask);
}
bool debug = false;

void ParticleFilter::reinit(cv::Mat image, cv::Mat templ, cv::Mat back_mask)
{
    using namespace std;
    using namespace cv;
    vector <double> reinit_weights(objects_size.size(), -1);
    vector <pair <int, int> > coords(objects_size.size());
    for (int i = 0; i < image.rows; ++i)
    {
        for (int j = 0; j < image.cols; ++j)
        {
            int obj_id = objects.at<int>(i, j);
            if (obj_id != 0)
            {
                if (reinit_weights[obj_id - 1] == -1)
                {
                    Mat img_hist = getObjectHist(objects.at<int>(i, j), image, back_mask);
                    Mat puck_hist = getHist(templ, this->mask);
                    double hist_dist = compareHist(img_hist, puck_hist, CV_COMP_BHATTACHARYYA);
                    int obj_size = objects_size[obj_id - 1];
                    double size_dist;
                    if (obj_size > SIZE_THRESHOLD * template_size || obj_size < SIZE_THRESHOLD / double(template_size))
                        size_dist = 0;
                    else
                        size_dist = template_size / fabs(template_size * SIZE_THRESHOLD - obj_size + EPS);
                    coords[obj_id - 1] = make_pair(i, j);
                    reinit_weights[obj_id - 1] = 1 - hist_dist + size_dist;
                }
            }
        }
    }

    int best_ind = -1;
    double best_weight = -1;
    for (int i = 0; i < reinit_weights.size(); ++i)
    {
        if (reinit_weights[i] > best_weight)
        {
            best_weight = reinit_weights[i];
            best_ind = i;
        }
    }
    
    poses.clear();
    
    vector <Mat> reinit_poses;
    Mat pose = Mat(4, 1, CV_64F);
    pose.at<double>(0) = 1070;
    pose.at<double>(1) = 1800;
    pose.at<double>(2) = 0;
    pose.at<double>(3) = 0;
    reinit_poses.push_back(pose);
    init(reinit_poses, nParticles, templ);

    
    namedWindow("Reinit", CV_WINDOW_NORMAL);
    imshow("Reinit", templ);
}

void ParticleFilter::evaluateSegment(cv::Mat const & image, cv::Mat const & templ, cv::Mat const & mask)
{
    using namespace cv;
    segmentObjects(mask);

    for (int i = 0; i < nParticles; ++i)
    {
        int r = poses[i].at<double>(0);
        int c = poses[i].at<double>(1);
        if (objects.at<int>(r, c) != 0)
        {
            Mat img_hist = getObjectHist(objects.at<int>(r, c), image, mask);
            Mat puck_hist = getHist(templ, this->mask);
            double hist_dist = compareHist(img_hist, puck_hist, CV_COMP_BHATTACHARYYA);
            int obj_size = objects_size[objects.at<int>(r, c) - 1];
            double size_dist;
            if (obj_size > SIZE_THRESHOLD * template_size || obj_size < SIZE_THRESHOLD / double(template_size))
                size_dist = 0;
            else
                size_dist = template_size / fabs(template_size * SIZE_THRESHOLD - objects_size[objects.at<int>(r, c) - 1] + EPS);
            weights[i] = 1 - hist_dist + size_dist;

            if (debug)
            {
                int obj_id = objects.at<int>(r, c);
                int x1 = objects_bboxes[obj_id - 1].first.x,
                    y1 = objects_bboxes[obj_id - 1].first.y,
                    x2 = objects_bboxes[obj_id - 1].second.x,
                    y2 = objects_bboxes[obj_id - 1].second.y;
                Rect bbox = Rect(Point(y1, x1), Point(y2, x2));
                Mat subregion = Mat(image, bbox).clone();
                Mat massk = Mat(mask, bbox).clone();
                Mat tmp = Mat::zeros(subregion.size(), subregion.type());
                subregion.copyTo(tmp, massk);
                namedWindow("test", CV_WINDOW_NORMAL);
                imshow("test", tmp);
                std::cout << hist_dist << std::endl;
                waitKey();
            }
        }
    }
    debug = false;
}

void ParticleFilter::segmentObjects(cv::Mat mask)
{
    using namespace cv;
    int n = mask.rows,
        m = mask.cols;
    objects = cv::Mat::zeros(mask.rows, mask.cols, CV_32S);
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            if (mask.at<char>(i, j) != 0 && objects.at<int>(i, j) == 0)
            {
                int cur_size = getObjectSize(mask, Point(i, j));
                objects_size.push_back(cur_size);
            }
        }
    }
}
void ParticleFilter::evaluateViaMOG(cv::Mat const & image, cv::Mat const & templ, cv::Mat const & mask)
{
    using namespace cv;
    segmentObjects(mask);
    for (size_t i = 0; i < poses.size(); ++i)
    {
        Mat subregion = poseToSubregion(poses[i], templ, mask);
        double w = getBackgroundDistance(subregion, poses[i]);
        // CONTROL CONSTANT: weight of MOG
        weights[i] = w * MOG_WEIGHT;
    }
}

void ParticleFilter::cumulativeResample()
{
    using namespace std;
    vector < double > cumulative_w(nParticles);
    vector <cv::Mat> new_poses(nParticles);

    cumulative_w[0] = std::pow(weights[0], 1.0);
    for (size_t i = 1; i < nParticles; ++i)
    {
        cumulative_w[i] = cumulative_w[i - 1] + std::pow(weights[i], 1.0);
    }

    /* Cumulative_w is non-increasing now. Let's emulate non-uniform distribution, based on weights:
    Let `sum' be total sum of all weights (note that it stored in last cumulative_w)
    Now we'll get uniformly random number from range of [0, sum] nParticles times,
    and find lower bound of that number in cumulative_w. Then next item is what we choose as result of roll. */
    for (size_t i = 0; i < nParticles; ++i)
    {
        double roll = rng.uniform(0.0, cumulative_w.back() - EPS);
        int lower_bound_ind = lower_bound(cumulative_w.begin(), cumulative_w.end(), roll) - cumulative_w.begin();
        new_poses[i] = poses[lower_bound_ind].clone();

        // CONTROL CONSTANT: inertia of particles cloud
        if (SPEED_HEURISTIC_ON)
        {
            if (lower_bound_ind > 3 * nParticles / 4)
            {
                new_poses[i].at<double>(2) = int(getNoise());
                new_poses[i].at<double>(3) = int(getNoise());
            }
        }
    }
    poses = new_poses;
    weights.assign(nParticles, 0.0);
    objects_size.clear();
    objects = cv::Mat::zeros(objects.rows, objects.cols, CV_32F);
    objects_bboxes.clear();
}

void ParticleFilter::gaussianResample()
{
    using namespace std;
    vector <pair <double, int> > sorted_w(nParticles);  // pairs <weight of pose, index of pose>
    for (size_t i = 0; i < nParticles; ++i)
        sorted_w[i] = make_pair(weights[i], i);

    sort(sorted_w.begin(), sorted_w.end());
    reverse(sorted_w.begin(), sorted_w.end());

    /* we use the same idea as in cumulative resampling, but
    now we stick 'heavier' poses closer to zero and 
    gaussian distribution over [0; sum of all weights], centered at zero */
    for (size_t i = 1; i < nParticles; ++i)
        sorted_w[i].first += sorted_w[i - 1].first;

    vector <cv::Mat> new_poses(nParticles);
    for (size_t i = 0; i < nParticles; ++i)
    {
        double roll = std::fabs(rng.gaussian(sorted_w.back().first / 2.0));
        int lower_bound_ind = lower_bound(sorted_w.begin(), sorted_w.end(), make_pair(roll, 0)) - sorted_w.begin();
        new_poses[i] = poses[lower_bound_ind % nParticles].clone();

        // CONTROL CONSTANT: inertia of particles cloud
        if (SPEED_HEURISTIC_ON)
        {
            if (lower_bound_ind > nParticles / 5)
            {
                new_poses[i].at<double>(2) = int(getNoise());
                new_poses[i].at<double>(3) = int(getNoise());
            }
        }
    }
    poses = new_poses;
    weights.assign(nParticles, 0.0);
    objects_size.clear();
    objects = cv::Mat::zeros(objects.rows, objects.cols, CV_32F);
}

void ParticleFilter::resample()
{
    // CONTROL: resample type
    cumulativeResample();
    //gaussianResample();
}

ParticleFilter::ParticleFilter()
{
    rng = cv::RNG(1337);
}

int ParticleFilter::size()
{
    return nParticles;
}

cv::Mat ParticleFilter::getWeightedMin()
{
    double x = 0, y = 0;
    double sum_weights = 0;
    for (int i = 0; i < nParticles; ++i)
    {
        x += poses[i].at<double>(0) * weights[i];
        y += poses[i].at<double>(1) * weights[i];
        sum_weights += weights[i];
    }
    x /= sum_weights;
    y /= sum_weights;
    cv::Mat res(4, 1, CV_64F);
    res.at<double>(0) = x;
    res.at<double>(1) = y;
    res.at<double>(2) = 0;
    res.at<double>(3) = 0;
    return res;
}

cv::Mat ParticleFilter::getBestMatch()
{
    double best_weight = -1;
    int best_ind = -1;
    for (size_t i = 0; i < nParticles; ++i)
    {
        if (weights[i] > best_weight)
        {
            best_weight = weights[i];
            best_ind = i;
        }
    }
    best_match_ind = best_ind;
    return poses[best_ind];
}

double ParticleFilter::getNoise()
{
    // CONTROL: RNG type
    double roll = rng.gaussian(MAX_DIST);
    return roll;
}

cv::Mat poseToSubregion(cv::Mat const & pose, cv::Mat const & templ, cv::Mat const & img)
{
    using namespace cv;
    int x1 = pose.at<double>(0),
        y1 = pose.at<double>(1),
        x2 = x1 + templ.rows,
        y2 = y1 + templ.cols;

    // TODO: Fix that nightmare
    x1 = min(x1, img.rows);
    y1 = min(y1, img.cols);
    x2 = min(x2, img.rows);
    y2 = min(y2, img.cols);
    x1 = max(x1, 0);
    x2 = max(x2, 0);
    y1 = max(y1, 0);
    y2 = max(y2, 0);
    return Mat(img, Rect(Point(y1, x1), Point(y2, x2))).clone();
}

