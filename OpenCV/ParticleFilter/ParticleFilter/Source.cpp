#include "opencv2\opencv.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include "ParticleFilter.hpp"
#include "HistogramMatching.hpp"
#include <stdio.h>
#include <iostream>
#include <sstream>

#define DRAW_SHAPE_RECTANGLE 1
#define DRAW_SHAPE_CIRCLE 2
#define EVALUATE_WITH_SEGMENTATION 1
#define REINIT_AFTER_LOSS 0
#define STEP_SHOW 1

const int DILATON_SIZE     = 1; // dilation intensivity
const int SLOW_OUTPUT_RATE = 2; // multiply each frame by SLOW_OUTPUT_RATE to make output video easier to analyze

using namespace cv;
using namespace std;

Mat frame; 
Mat fgMaskMOG2; 
Ptr<BackgroundSubtractor> pMOG2; 
ParticleFilter PF;
int keyboard;

void drawPose(Mat & image, Mat const & templ, Mat const & pose, int shape)
{
    int x1 = static_cast<int>(pose.at<double>(0)),
        y1 = static_cast<int>(pose.at<double>(1)),
        x2 = x1 + templ.rows,
        y2 = y1 + templ.cols;
    switch (shape)
    {
    case DRAW_SHAPE_RECTANGLE:
        rectangle(image, cvPoint(x1, y1), cvPoint(x2, y2), Scalar(0, 255, 0), 0.5);
        break;
    case DRAW_SHAPE_CIRCLE:
        circle(image, cvPoint(y1, x1), 1, Scalar(0, 255, 0), 1);
        break;
    }
}

void splitVideoIntoImages()
{
    string video_file = "Cut_Compressed.mp4";
    VideoCapture capture(video_file);
    if (!capture.isOpened()) {
        cerr << "Unable to open video file: " << video_file << endl;
        exit(1);
    }
    int i = 0;
    while ((char)keyboard != 'q' && (char)keyboard != 27)
    {
        if (!capture.read(frame)) 
        {
            cerr << "Unable to read next frame." << endl;
            cerr << "Exiting..." << endl;
            exit(EXIT_FAILURE);
        }
        imwrite(to_string(i) + ".jpg", frame);
        ++i;
    }
}

void erodeBackground()
{
    Mat element = getStructuringElement(MORPH_RECT, Size(2 * DILATON_SIZE + 1, 2 * DILATON_SIZE + 1), Point(DILATON_SIZE, DILATON_SIZE));
    dilate(fgMaskMOG2, fgMaskMOG2, element);
}

void processVideo()
{
    //==================HARDCODED VALUES FOR DEVELOPING===================
    string template_file = "Puck.jpg";
    string videoFilename = "Cut_Compressed.mp4";
    String outputFilename = "Output4.avi";
    int initPuckX = 769;
    int initPuckY = 992;

    //====================================================================
    Mat puck = imread(template_file);
    namedWindow("Result", CV_WINDOW_NORMAL);
    namedWindow("Debug", CV_WINDOW_NORMAL);

    // init Particle Filter
    pMOG2 = createBackgroundSubtractorMOG2(500, 150, true);
    vector<Mat> init_vector;
    Mat t = Mat(4, 1, CV_64F);
    t.at<double>(0) = initPuckX;
    t.at<double>(1) = initPuckY;
    t.at<double>(2) = 0.0;
    t.at<double>(3) = 0.0;
    init_vector.push_back(t);
    PF.init(init_vector, 300, puck);
    VideoCapture capture(videoFilename);
    VideoWriter output(outputFilename, CV_FOURCC('M','J','P','G'), 25.0, Size(1920, 1080));
    capture.set(CV_CAP_PROP_FPS, 25);
    if (!capture.isOpened()) {
        cerr << "Unable to open video file: " << videoFilename << endl;
        exit(1);
    }
    RNG r(1488);
    int I = 0;
    while ((char)keyboard != 'q' && (char)keyboard != 27) 
    {
        ++I;
        if (!capture.read(frame)) {
            cerr << "Unable to read next frame." << endl;
            cerr << "Exiting..." << endl;
            exit(EXIT_FAILURE);
        }

        // This ensures that puck template will be in same color system as video
        if (I == 1)
        {
            puck = poseToSubregion(t, puck, frame);
        }

        // Prediction
        PF.predict();
        pMOG2->apply(frame, fgMaskMOG2);

        // Evaluation particles
        if (EVALUATE_WITH_SEGMENTATION)
        {
            PF.evaluateSegment(frame, puck, fgMaskMOG2);
        }
        else
        {
            PF.evaluateViaMOG(frame, puck, fgMaskMOG2);
            PF.evaluateViaHists(frame, puck);
        }
        PF.getBestMatch();

        // Reinitialization
        if (REINIT_AFTER_LOSS)
        {
            if (PF.weights[PF.best_match_ind] < 0.2 && I > 10)
            {
                PF.reinit(frame, puck, fgMaskMOG2);
                std::cout << "Reinit!" << std::endl;
                waitKey();
                continue;
            }
        }

        // Printing result
        Mat debug_image = fgMaskMOG2.clone();
        cvtColor(debug_image, debug_image, CV_GRAY2BGR);
        for (size_t j = 0; j < PF.poses.size(); ++j)
        {
            drawPose(frame, puck, PF.poses[j], DRAW_SHAPE_CIRCLE);
        }
        Mat best = PF.getWeightedMin();
        int x = best.at<double>(0);
        int y = best.at<double>(1);
        circle(frame, Point(y, x), 10, Scalar(0, 0, 255), 2);
        if (STEP_SHOW)
        {
            imshow("Result", frame);
            imshow("Debug", debug_image);
            waitKey();
        }
        else
        {
            for (size_t i = 0; i < SLOW_OUTPUT_RATE; ++i)
                output.write(frame);
        }

        // Resample for next iteration
        PF.resample();
    }
    output.release();
    capture.release();
}

int main(int argc, char* argv[])
{
    processVideo();
    return 0;
}

