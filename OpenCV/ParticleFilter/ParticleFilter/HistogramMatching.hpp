#pragma once
#include "opencv2\world.hpp"
#include "opencv2\imgproc.hpp"

double getHistDistance(cv::Mat input_img, cv::Mat input_template, cv::Mat mask);
cv::MatND getHist(cv::Mat const & region, cv::Mat mask);