#include "HistogramMatching.hpp"
#include "opencv2\world.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\imgproc.hpp"
#include <iostream>


using namespace cv;
using namespace std;
MatND getHist(Mat const & src, Mat mask)
{
    int h_bins = 50,
        s_bins = 60;
    int histSize[] = { h_bins, s_bins };
    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    MatND hist;
    const float* ranges[] = { h_ranges, s_ranges };

    int channels[] = { 0, 1 };

    /// Compute the histograms:
    calcHist(&src, 1, channels, mask, hist, 2, histSize, ranges, true, false);
    normalize(hist, hist, 0, 1, NORM_MINMAX, -1, Mat());
    return hist;
}

double getHistogramDifference(MatND const & hist_img, MatND const & hist_template)
{
    return compareHist(hist_img, hist_template, CV_COMP_BHATTACHARYYA);
}

void showHistogram(Mat& img);
double getHistDistance(cv::Mat img, cv::Mat tpl, cv::Mat mask)
{
    Mat input_img = Mat::zeros(img.size(), img.type());
    img.copyTo(input_img, mask);
    Mat input_template = Mat::zeros(tpl.size(), tpl.type());
    tpl.copyTo(input_template, mask);
    cvtColor(input_img, input_img, COLOR_BGR2HSV);
    cvtColor(input_template, input_template, COLOR_BGR2HSV);
    
    MatND hist_img, hist_template;
    hist_img = getHist(input_img, mask);
    hist_template = getHist(input_template, mask);
    
    double res = getHistogramDifference(hist_img, hist_template);
    //res /= 0.546;
    /*cv::namedWindow("test1", CV_WINDOW_NORMAL);
    cv::imshow("test1", input_img);
    cv::namedWindow("test2", CV_WINDOW_NORMAL);
    cv::imshow("test2", input_template);
    std::cout << res << std::endl;
    waitKey();*/

    return res;
}

void showHistogram(Mat& img)
{
    int bins = 256;             // number of bins
    int nc = img.channels();    // number of channels

    vector<Mat> hist(nc);       // histogram arrays

                                // Initalize histogram arrays
    for (int i = 0; i < hist.size(); i++)
        hist[i] = Mat::zeros(1, bins, CV_32SC1);

    // Calculate the histogram of the image
    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            for (int k = 0; k < nc; k++)
            {
                uchar val = nc == 1 ? img.at<uchar>(i, j) : img.at<Vec3b>(i, j)[k];
                hist[k].at<int>(val) += 1;
            }
        }
    }

    // For each histogram arrays, obtain the maximum (peak) value
    // Needed to normalize the display later
    int hmax[3] = { 0,0,0 };
    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < bins - 1; j++)
            hmax[i] = hist[i].at<int>(j) > hmax[i] ? hist[i].at<int>(j) : hmax[i];
    }

    const char* wname[3] = { "blue", "green", "red" };
    Scalar colors[3] = { Scalar(255,0,0), Scalar(0,255,0), Scalar(0,0,255) };

    vector<Mat> canvas(nc);

    // Display each histogram in a canvas
    for (int i = 0; i < nc; i++)
    {
        canvas[i] = Mat::ones(125, bins, CV_8UC3);

        for (int j = 0, rows = canvas[i].rows; j < bins - 1; j++)
        {
            line(
                canvas[i],
                Point(j, rows),
                Point(j, rows - (hist[i].at<int>(j) * rows / hmax[i])),
                nc == 1 ? Scalar(200, 200, 200) : colors[i],
                1, 8, 0
                );
        }

        imshow(nc == 1 ? "value" : wname[i], canvas[i]);
    }
}